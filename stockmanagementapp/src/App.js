import Paperbase from "./paperbase/Paperbase";

function App(){
    return(
        <div>
            <Paperbase />
        </div>
    )
}

export default App;