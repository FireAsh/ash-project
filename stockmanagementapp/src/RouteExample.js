import {AppBar, Drawer, Toolbar, Typography, List, ListItem} from "@mui/material"
import {BrowserRouter, Link, Routes, Route} from "react-router-dom";
import ContactForm from "./Components/Form/ContactForm";
import ContactCardGrid from "./Components/Form/ContactCardGrid";
import ContactTable from "./Components/Form/ContactTable";
import ContactDataGrid from "./Components/Form/ContactDataGrid";


function RouteExample() {
  return (
    <BrowserRouter>
    <div className="App">
      <AppBar position="fixed" sx={{zIndex: 9999}}>
          <Toolbar>
            <Typography variant="h6" noWrap>
              Advanced Material UI Styling
            </Typography>
          </Toolbar>
      </AppBar>
      <Drawer variant="temporary" open={true}>
          <List>
            {[{text: "Input Form", route: '/form'}, {text: "Contact Card Grid", route: '/grid'}, {text: "Contact Table", route: '/table'}, {text: "Contact Data Grid", route: '/datagrid'}].map((nav, index) => (
              <ListItem key = {nav.text}><Link to={nav.route}>{nav.text}</Link></ListItem>))}
          </List>
      </Drawer>
      <main>
            <Routes>
              <Route path={"/"} element={<ContactForm />}/>
              <Route path={"/form"} element={<ContactForm/>}/>
              <Route path={"/grid"} element={<ContactCardGrid />}/>
              <Route path={"/table"} element={<ContactTable />}/>
              <Route path = {"/datagrid"} element={<ContactDataGrid />} />
            </Routes>
      </main>
    </div>
    </BrowserRouter>
  );
}

export default RouteExample;
