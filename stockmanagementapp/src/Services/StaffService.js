import axios from "axios";

const REST_API_BASE_URL = 'http://localhost:8080/api/staffs/getallstaffs';

export const listStaffs = () => {
    return axios.get(REST_API_BASE_URL);
}