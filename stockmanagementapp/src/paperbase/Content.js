import * as React from 'react';
import Button from '@mui/material/Button';
import Box from '@mui/system/Box';
import Annoying from '../Components/annoyingcomponent'


export default function Content({title, thecoachtype, list}) {

 
  return (
    <>
    {(title == 'Authentication' || title == 'Database') && <Annoying placeholdername = {`Search for ${thecoachtype} by email address, phone number, or user UID`} list={list}/>}

    {title === 'Find a Coach' && <Annoying placeholdername={`Search for ${thecoachtype}`} list={list}/>}
    
    <p></p>
    <Box m={1}
      display="flex"
      justifyContent="flex-end">
    {title === 'Database' &&<Button variant="contained" sx={{height: 40}}>
          Add user
    </Button>}
    </Box>
    </>
  );
}
