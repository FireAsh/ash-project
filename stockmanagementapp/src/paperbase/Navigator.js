import * as React from 'react';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import Box from '@mui/material/Box';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import HomeIcon from '@mui/icons-material/Home';
import PeopleIcon from '@mui/icons-material/People';
import DnsRoundedIcon from '@mui/icons-material/DnsRounded';
import SportsTennisIcon from '@mui/icons-material/SportsTennis';
import SportsSoccerIcon from '@mui/icons-material/SportsSoccer';
import FlightIcon from '@mui/icons-material/Flight';
import {Link, Route, Routes} from "react-router-dom";
import { useState } from 'react';
import Button from './Button';





const item = {
  py: '2px',
  px: 3,
  color: 'rgba(255, 255, 255, 0.7)',
  '&:hover, &:focus': {
    bgcolor: 'rgba(255, 255, 255, 0.08)',
  },
};

const itemCategory = {
  boxShadow: '0 -1px 0 rgb(255,255,255,0.1) inset',
  py: 1.5,
  px: 3,
};

export default function Navigator(props) {
  const [makeactive, setactive] = useState({
    a: false,
    b: false,
    c: false,
    d: false,
    e: false
  });

  const { ...other } = props;

  const categories = [
    {
      id: 'Services',
      children: [
        {
          id: 'Authentication',
          icon: <PeopleIcon />,
          letter: 'a',
          active: makeactive.a,
          
        },
        { id: 'Find a Coach', 
          icon: <SportsTennisIcon />,
          letter: 'b',
          active: makeactive.b, 
          
        },
        { id: 'Latest Football News', 
          icon: <SportsSoccerIcon/>,
          letter: 'c',
          active: makeactive.c,
         
        },
        { id: 'Travel', 
          icon: <FlightIcon/>,
          letter: 'd',
          active: makeactive.d,
          
        },
        { id: 'Database',
          icon: <DnsRoundedIcon />,
          letter: 'e',
          active: makeactive.e,
          
        }
      ],
    },
   
  ];

  function changeOnClick(letter){
    switch(letter){
      // case a
      case 'a': setactive((prevState) => ({
        ...false,
        a : !prevState.a, // or true
       }));
       break;
       
      // case b
      case 'b': setactive((prevState) => ({
        ...false,
        b : !prevState.b, // or true
       }));
       break;

      // case c
      case 'c': setactive((prevState) => ({
        ...false,
        c : !prevState.c, // or false 
       }));
       break;

      // case d
       case 'd': setactive((prevState) => ({
        ...false,
        d : !prevState.d, // or false 
       }));
       break;

       // case e
       case 'e': setactive((prevState) => ({
        ...false,
        e : !prevState.e, // or false 
       }));
       break;
    }

  }

  


  return (
      <Drawer variant="permanent" {...other}>
        <List disablePadding>
          <ListItem sx={{ ...item, ...itemCategory, fontSize: 22, color: '#fff' }}>
            Sports and Travel Management Application
          </ListItem>

          <Link style={{textDecoration: 'none'}} to={'/home'} >
          <ListItem sx={{ ...item, ...itemCategory }}>
            <ListItemIcon>
              <HomeIcon />
            </ListItemIcon>
            <ListItemText>Homepage</ListItemText>
          </ListItem>
          </Link>
          {categories.map(({ id, children }) => (
            <Box key={id} sx={{ bgcolor: '#101F33' }}>
              <ListItem sx={{ py: 2, px: 3 }}>
              
                <ListItemText sx={{ color: '#fff' }}>{id}</ListItemText>

              </ListItem>
              {children.map(({ id: childId, icon, active, letter }) => (
              
                <Link style={{textDecoration: 'none'}}  key={"Link "+childId}>
                  <ListItem disablePadding key={childId} onClick={() => changeOnClick(letter)}>
                    <ListItemButton selected={active} sx={item} onClick={() => props.onClickServices(childId)}>
                      <ListItemIcon>{icon}</ListItemIcon>
                      <ListItemText>{childId}</ListItemText>
                    </ListItemButton>
                  </ListItem>
                </Link>

              ))}
              <Divider sx={{ mt: 2 }} />
            </Box>
          ))}
        </List>
        {/*<main>
            <Routes>
              <Route path={"/home"} />
              <Route path={"/authenticate"} />
              <Route path={"/findcoach"} />
              <Route path={"/footballleagues"} />
              <Route path = {"/attractionstovisit"}  />
              <Route path = {"/database"} />
            </Routes>
              </main>*/}
      </Drawer>

  );
}
